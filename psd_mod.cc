/*
psd_mod.cc

PSD models.
*/

#include "psd_mod.h"

/*
    Function: psd_brkn_powerlaw
    Calculates broken power law model.
    Input:
    f - array of frequencies at which to calculate the model
    p - model parameters [P_break, f_break, a1, a2, c]
    L - length of model
    model - output array of PSD values
*/
int psd_brkn_powerlaw(double* f, double* p, int L, double* model)
{
    using namespace std;

    double k1 = (p[0] - p[4]) / pow(p[1], -p[2]);
    double k2 = (p[0] - p[4]) / pow(p[1], -p[3]);
    for (int i=0; i < L; i++)
    {
        model[i] = (f[i] <= p[1]) ?
            k1*pow(f[i], -p[2]) + p[4] :
            k2*pow(f[i], -p[3]) + p[4];
    }
    return 0;
}

