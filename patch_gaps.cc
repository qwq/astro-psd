/*
patch_gaps.cc

For patching data gaps in lightcurve.
*/

#include "patch_gaps.h"


using std::malloc;

/*
Function: find_gaps
    Find array indices of gaps in array x.
    Input:
        x - data series
        L - length of x
    Output:
        gaps - indices of gaps
        return value - size of gaps
*/
int find_gaps(double* x, int L, int** gaps)
{
    int ct=0;
    for (int i=0; i<L; i++)
        if (x[i] == 0) ct++;
    int* gapInx = static_cast<int*>(malloc(sizeof(int)*ct));
    int k=0;
    for (int i=0; i<L; i++)
        if (x[i] == 0) gapInx[k++] = i;
    *gaps = gapInx;
    return ct;
}

/*
Function: patch_line
    Join gaps by linear interpolation.
    Input:
     x  abscissa
     y  function with gaps; on output gaps are overwritten
     flag   y value of gaps
     L  length of the arrays x and y
*/
int patch_line(double* x, double* y, double flag, int L)
{
    int gapInx=0;
    int gapLength=0;
    int ct=0;
    double dx, dy, gr;
    for (int i=0; i<L; i++)
    {
        if (y[i]==flag)
        {
            gapLength++;
        }
        else
        {
            if (gapLength>0)
            {
                dx=x[i]-x[gapInx];
                dy=y[i]-y[gapInx];
                gr=dy/dx;
                for (int j=1; j<=gapLength; j++)
                {
                    y[gapInx+j]=y[gapInx]+gr*(x[gapInx+j]-x[gapInx]);
                }
                ct+=gapLength;
                gapLength=0;
            }
            gapInx=i;
        }
    }
    return ct;
}

