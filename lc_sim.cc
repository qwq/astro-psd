#include "lc_sim.h"

double lc_sim(sim_config* simcf, model_config* modcf) {
    // ******
    // Read config
    double *p = modcf->pars;  // copy pointer to model pars
    double tbin = simcf->tbin;
    int sim_max = simcf->sim_max;
    int rndsd = simcf->rndsd;
    int rnl = simcf->rnl;
    bool batchmode = simcf->batchmode;
    bool output_ps = simcf->output_ps;
    bool output_chisq = simcf->output_chisq;
    bool output_psdist = simcf->output_psdist;
    bool output_inpsd = simcf->output_inpsd;
    bool use_window = simcf->use_window;
    bool pdfadjust = simcf->pdfadjust;
    const char* lcfile = simcf->lcfile;
    const char* lcpdffile = simcf->lcpdffile;
    // ******


    //*************************************************
    // Test output files first so we don't waste effort
    FILE *outfile_ps;
    FILE *outfile_chisq;
    FILE *outfile_psdist;
    FILE *outfile_inpsd;
    if (output_ps) outfile_ps = prep_outfile("output/sim_psd.dat");
    if (output_chisq) outfile_chisq = prep_outfile("output/sim_chisq.dat");
    if (output_psdist) outfile_psdist = prep_outfile("output/sim_psdist.dat");
    if (output_inpsd) outfile_inpsd = prep_outfile("output/obs_inpsd.dat");
    //*************************************************


    //*************************************************
    // Read in obs lc data
    double* lct;
    double* lcf;
    int lcl = read_lc_fits(lcfile, &lct, &lcf);  // lightcurve length
    //*************************************************


    //*************************************************
    // Taper windows
    double *wnd;
    if (use_window)
        wnd = (*(simcf->wnd_func))(lcl);
    //*************************************************


    //*************************************************
    // Calculate mean and stdev of non-zero obs data
    double lcmean = 0;
    double lcstdev = 0;
    int counter = 0;
    for (int i=0; i < lcl; i++) {
        if (lcf[i] > 0) {
            counter++;
            lcmean += lcf[i];
            lcstdev += lcf[i]*lcf[i];
        }
    }
    lcmean /= 1.0*counter;
    lcstdev = sqrt(lcstdev/counter - lcmean*lcmean);
    if (!batchmode)
        printf("%d valid data points, mean %e counts/s, stdev %e counts/s\n",
                                    counter, lcmean, lcstdev);

    //*************************************************


    //*************************************************
    // Locate the gaps and count, for later use
    int* gaps;
    int gaps_count = find_gaps(lcf, lcl, &gaps);
    //*************************************************


    //*************************************************
    // Patch the gaps
    (*(simcf->gap_func))(lct, lcf, 0.0, lcl);
    //*************************************************


    //*************************************************
    // For adjusting lightcurve PDF
    if (pdfadjust)
        CDFTable pdf_gen(lcpdffile);
    //*************************************************


    //*************************************************
    // Prepare FFTW transforms
    // Import FFTW wisdom
    fftw_import_wisdom_from_filename("lc_sim.1hr.wisdom");
    if (!fftw_import_wisdom_from_filename("lc_sim.wisdom")) {
        printf("Warning: FFTW Wisdom import failed!\n");
    }
    fftw_import_wisdom_from_filename("lc_sim.2hr.wisdom");
    fftw_import_wisdom_from_filename("lc_sim.4hr.wisdom");
    // Container for real FFT of lc
    int psl = floor(lcl/2.)+1;
    fftw_complex *fs = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex) * psl));
    double* ts = static_cast<double*>(fftw_malloc(sizeof(double) * lcl));
    // Create FFTW plans
    if (!batchmode) printf("Creating FFTW plans... this could take a while!\n");
    fftw_plan l2p = fftw_plan_dft_r2c_1d(lcl, ts, fs, FFTW_ESTIMATE);
    // fftw_plan p2l = fftw_plan_dft_c2r_1d(lcl, fs, ts, FFTW_MEASURE);
    //*************************************************


    //*************************************************
    // Get periodogram of observed data
    // Remove dc component from input
    double inputmean = mean(lcf, lcl);
    if (use_window) {
        for (int i=0; i < lcl; i++) ts[i] = (lcf[i] - inputmean) * wnd[i];
    } else {
        for (int i=0; i < lcl; i++) ts[i] = lcf[i] - inputmean;
    }
    fftw_execute(l2p);
    //*************************************************


    /**
     TK95 method starts here
    */
    // Unnormalized PSD model params (log10(norm) = 4.5710295640855252)


    // Compute settings for simulation
    int sflen;                           // Positive frequencies
    sflen = floor(rnl*lcl/2);            // Even: N/2 positive freqs
                                         // Odd: (N-1)/2 positive freqs
    double freq1 = 1.0/(rnl*lcl*tbin);


    //*************************************************
    // Allocate memory for simulation data
            // Simulated long lightcurve from PSD amplitude + rand phases
    double* freq = static_cast<double*>(malloc(sizeof(double)*sflen));
    fftw_complex *sim_ps_full =          // +1 for dc component
                  static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex)*(sflen+1)));
    double *sim_lc_full = static_cast<double*>(fftw_malloc(sizeof(double)*rnl*lcl));
    double *ampl = static_cast<double*>(malloc(sizeof(double)*sflen));
            // Lightcurve segments
    double *lc_out = static_cast<double*>(fftw_malloc(sizeof(double)*lcl));
            // Periodogram of segments
    fftw_complex *ps_out = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex)*psl));
            // ..and amplitudes
    double* ps_out_ampl = static_cast<double*>(malloc(sizeof(double)*psl));
    double* pdfsim_lc;
    double* pdfsim_lc_sorted;
    int* pdfsim_lc_rank;
    fftw_complex* pdfsim_ps;
    if (pdfadjust) {       // White noise simulation w/ observed PDF
        pdfsim_lc = static_cast<double*>(fftw_malloc(sizeof(double)*lcl));
            // Ranked (smallest first) for replacement values
        pdfsim_lc_sorted = static_cast<double*>(malloc(sizeof(double)*lcl));
            // Ranking (for adjustments)
        pdfsim_lc_rank = static_cast<int*>(malloc(sizeof(int)*lcl));
            // DFT of white noise simulation
        pdfsim_ps = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex)*psl));
    }
            // For collating simulation periodograms
    double* ps_meanlog = static_cast<double*>(calloc(psl, sizeof(double)));
    double* ps_errlog = static_cast<double*>(calloc(psl, sizeof(double)));
    double lc_chisq;  // chisq of observed data periodogram
    double* lc_chisq_contrib = static_cast<double*>(malloc(psl*sizeof(double)));
    double **bigpstore = static_cast<double**>(malloc(sim_max*(sizeof(double*))));
    for (int i=0; i < sim_max; i++)  // store periodogram instances
        bigpstore[i] = static_cast<double*>(malloc(psl*sizeof(double)));
    //*************************************************


    //*************************************************
    // Prepare FFTW plans (must do this before filling values)
    // printf("Planning FFTW transforms... ");
    fftw_plan sim_p2l =
        fftw_plan_dft_c2r_1d(rnl*lcl, sim_ps_full, sim_lc_full, FFTW_MEASURE);
    fftw_plan sim_l2p =
        fftw_plan_dft_r2c_1d(lcl, lc_out, ps_out, FFTW_MEASURE);
    /*fftw_plan pdf_l2p =
        fftw_plan_dft_r2c_1d(lcl, pdfsim_lc, pdfsim_ps, FFTW_MEASURE);
    fftw_plan pdf_p2l =
        fftw_plan_dft_c2r_1d(lcl, pdfsim_ps, pdfsim_lc, FFTW_MEASURE);
        */
    // Save FFTW wisdom
    if (!batchmode)  // Skip this step when in batch mode
        if (!fftw_export_wisdom_to_filename("lc_sim.wisdom"))
            printf("Warning: FFTW Wisdom was not saved!\n");
    //*************************************************

    // Generate amplitudes from model & initial setup
    for (int i=0; i < sflen; i++) {
        freq[i] = freq1*(i+1);
    }
    (*(modcf->func))(freq, p, sflen, ampl);


    for (int i=0; i < sflen; i++) {  // split between + and - freqs and norm
        ampl[i] = sqrt(0.5*(ampl[i]));
    }

    if (!batchmode) {
        printf("\rBeginning simulations...! "
                "Running %d realizations per model -\n", sim_max);
        int memdemand;
        memdemand = sim_max*psl*sizeof(double)/1048576;
        printf("About %d MB of memory needed to store periodogram data.\n",
                                            memdemand);
    }

    int pcounter = 0;



    //*************************************************
    // Do work
    double c_start = omp_get_wtime();  // Start timer
    while (pcounter < sim_max) {
        // Randomize phases & Multiply into amplitudes from model
        rndsd++;  // Don't use pcounter - may get stuck in infinite loop
        srand(rndsd);
        std::complex<double> rndphase;

        int seed = rand();
        // if (pcounter==80305) printf("%d\n",seed);
        // Set dc component to 0 - needs to be refilled every time
        // otherwise it can change...
        sim_ps_full[0][0] = 0;
        sim_ps_full[0][1] = 0;
        for (int i=1; i <= sflen; i++) {  // Fill from 2nd to sflen+1'th element
            rndphase = c8_normal_01(seed);
            sim_ps_full[i][0] = real(rndphase) * ampl[i-1];
            sim_ps_full[i][1] = imag(rndphase) * ampl[i-1];
            // if (pcounter==80305) printf("%.8e\t%.8e\n",
            //  sim_ps_full[i][0], sim_ps_full[i][1]);
        }

        fftw_execute(sim_p2l);  // Take inverse DFT to get full lightcurve

        // if (pcounter==80305) printf("%d\t%d\t%d *************8\n",
        //                  rndsd,seed,std::isnan(sim_lc_full[1]));

        /*
            if (pcounter==80305) // debug - print full lightcurve for id
            {
                for (int jj=0;jj < lcl*rnl;jj++)
                    printf("%.8e\n",sim_lc_full[jj]);
                cout << "***********************************" << endl;
                return 1;
            }
        */
        //******************************************************
        //*** Catch NaN errors in FFTW results...
        //*** Sometimes a legit input
        //    sim_ps_full goes in, fftw_execute runs, and
        //    the output in sim_lc_full is filled with NaNs.
        //    What is it doing??
        //    Must throw the entire long lc out and try again.
        //  Try to reproduce with rndsd = 10000 initially,
        //  sim_max = 10000.
        //  Could this have been due to the 0th element containing
        //  spurious data? (This has since been made to be assigned 0
        //  every time...)
            if (std::isnan(sim_lc_full[1])) continue;
        //******************************************************


        //*************************************************
        // Long lightcurve has valid data, proceed to chop
        // it up into rnl x segments length lcl
        for (int li=0; li < rnl; li++) {  // to rnl
            if (pcounter >= sim_max) break;  // stop at desired iterations

            // Get segment
            for (int i=0; i < lcl; i++) lc_out[i] = sim_lc_full[rnl*li+i];

            // Emulate observed window
            for (int i=0; i < gaps_count; i++) lc_out[gaps[i]] = 0;


            //*************************************************
            // Negative flux lightcurve rejection
            // (after scaling to the same mean and stdev as input lightcurve)
            double simlcmean = 0;
            double simlcstdev = 0;
            double lcscale;
            double lcoffset;
            bool skip = 0;
            for (int i=0; i < lcl; i++) {
                simlcmean += lc_out[i];  // gaps will be 0, won't contribute
                simlcstdev += lc_out[i]*lc_out[i];
            }
            simlcmean /= 1.0*(lcl-gaps_count);  // account for gaps
            simlcstdev = sqrt(simlcstdev/(lcl-gaps_count) - simlcmean*simlcmean);

            lcscale = lcstdev/simlcstdev;
            lcoffset = lcmean - simlcmean*lcscale;

            for (int i=0; i < lcl; i++) {
                if (lc_out[i] != 0) {  // not a gap
                    lc_out[i] = lc_out[i]*lcscale + lcoffset;
                    if (lc_out[i] < 0) {  // negative value...
                        skip = 1;
                        break;
                    }
                } else {
                    continue;
                }
            }

            //*************************************************
            //*************************************************
            // Skip to next segment if a negative value *******
                if (skip) continue;  //************************
            //*************************************************
            //*************************************************

            // Short-circuit for debugging
            // if (pcounter != 80266) {printf("%d\n",pcounter++); continue;}

            //*****************************************************************
            // Iteratively adjust (Emmanoulopoulos et al. 2013)
            // Zw229 isn't very burstly and subject to slow variation
            // and imposing the same observed PDF on simulated lightcurves
            // result in distinctly different features...
            // For now let the PDF be Gaussian and skip this - simply
            // discard simulations that have negative values
            /*
            // Take DFT of this to get the periodogram
            fftw_execute(sim_l2p);
            // Calculate amplitudes
            for (int i=0; i < psl; i++)
                ps_out_ampl[i] = sqrt(ps_out[i][0]*ps_out[i][0] +
                                ps_out[i][1]*ps_out[i][1]);

            // Generate white noise lightcurve with the right PDF
            pdf_gen.fillrandflux(pdfsim_lc, lcl);
            for (int i=0; i < lcl; i++) pdfsim_lc_sorted[i] = pdfsim_lc[i];
            sort(pdfsim_lc_sorted, pdfsim_lc_sorted+lcl);

            int countdown = 50;
            while (countdown--)
            {
                double ampl_ratio;
                // Get DFT of white noise and DFT of PSD lightcurve segment
                fftw_execute(pdf_l2p);

                // Replace white noise DFT amplitudes by PSD sim lightcurve
                for (int i=0; i < psl; i++)
                {
                    ampl_ratio = ps_out_ampl[i]/sqrt(
                                    pdfsim_ps[i][0]*pdfsim_ps[i][0] +
                                    pdfsim_ps[i][1]*pdfsim_ps[i][1]);
                    pdfsim_ps[i][0] *= ampl_ratio;
                    pdfsim_ps[i][1] *= ampl_ratio;
                    //printf("%.15e\t%.15e\t%.15e\n",
                    //       pdfsim_ps[i][0], pdfsim_ps[i][1], ampl_ratio);
                }

                // Take inverse transform of modified white noise DFT
                fftw_execute(pdf_p2l);

                // Sort by flux and replace fluxes
                rankcounts(pdfsim_lc, pdfsim_lc_rank, lcl);
                for (int i=0; i < lcl; i++)
                    pdfsim_lc[pdfsim_lc_rank[i]] = pdfsim_lc_sorted[i];
            }
            // End iterative adjustments to lightcurve's PDF
            */
            //*****************************************************************

            // for (int i=0; i < lcl; i++)
            //  printf("%.15e\n",pdfsim_lc[i]);


            //*****************************************************************


            // Process simulated lightcurve before making periodogram
            (*(simcf->gap_func))(lct, lc_out, 0.0, lcl);  // Interpolate over gaps
            // Find dc component and remove it before taking transform
            inputmean = mean(lc_out, lcl);
            if (use_window) {
                for (int i=0; i < lcl; i++)  // Remove dc component
                    lc_out[i] = (lc_out[i]-inputmean)*wnd[i];
            } else {
                for (int i=0; i < lcl; i++)
                    lc_out[i] -= inputmean;  // Remove dc component
            }


            fftw_execute(sim_l2p);  // Take DFT


            for (int i=0; i < psl; i++) {  // Add this periodogram to the stack
                double amplsq0 = ps_out[i][0]*ps_out[i][0] +
                                       ps_out[i][1]*ps_out[i][1];
                double amplsq = log10(amplsq0);
                ps_meanlog[i] += amplsq;  // Collect statistic...
                ps_errlog[i] += amplsq*amplsq;
                bigpstore[pcounter][i] = amplsq;  // Also store amplitude^2

/*              if (i%1000 == 0 && pcounter==7723)  // debugging use
                {
                    printf("%d\t%d\t%.8e\t%.8e\t%.8e\t%.8e\n",
                    pcounter,i, ps_meanlog[i], ps_errlog[i], amplsq0, amplsq);
                    for (int jj=0;jj < lcl;jj++)
                        printf("%.8e\n",lc_out[jj]);
                }
*/
            }
            pcounter++;
            // printf("\r%d       ",pcounter);

            // Progress indicator
            if (!batchmode) {
                if (pcounter % (sim_max/100) == 0) {
                    double progress = 1.*pcounter/sim_max;
                    printf("\r%.3f%%   - %ld s\n",
                        100*progress,
                        (int64_t) ((omp_get_wtime()-c_start)/progress*(1-progress)));
                }
            }
        }
    }

    double normalisation = log10(2.0*tbin/(lcl*lcmean*lcmean));
    for (int i=0; i < psl; i++) {
        double pres, ps_errsq;
        ps_meanlog[i] /= 1.0*pcounter;
        ps_errsq = ps_errlog[i]/pcounter - ps_meanlog[i]*ps_meanlog[i];
        ps_errlog[i] = sqrt(ps_errsq);

        // printf("%d\t%.8e\t%.8e\t%.8e\n",
        //      i, ps_meanlog[i], ps_errlog[i], ps_errsq);  // debugging

        // Work out chisq contrib for obs periodogram
        lc_chisq_contrib[i] = log10(fs[i][0]*fs[i][0]+fs[i][1]*fs[i][1]);
        lc_chisq_contrib[i] -= ps_meanlog[i];
        lc_chisq_contrib[i] *= lc_chisq_contrib[i]/ps_errsq;

        // Statistics for realizations
        for (int ii=0; ii < sim_max; ii++) {
            pres = bigpstore[ii][i]-ps_meanlog[i];  // residual for this PS
            bigpstore[ii][i] = pres/ps_errlog[i];  // res/err (signed)
        }
        ps_meanlog[i] += normalisation;  // correct normalisation last!
    }


    freq1 = 1.0/(tbin*lcl);  // for the short (observed lcl)
    // Print out the periodogram of input data
    if (output_inpsd) {
        double psdval;

        for (int i=0; i < psl; i++) {
            psdval = log10(fs[i][0]*fs[i][0]+fs[i][1]*fs[i][1]);
            psdval += normalisation;
            fprintf(outfile_inpsd, "%.8e\t%.8e\n", freq1*i, psdval);
        }
        fclose(outfile_inpsd);
    }

    lc_chisq = 0;  // reset obs lc chisq
    // Calculate obs chisq under this model
    for (int i=0; i < psl; i++)
        lc_chisq += lc_chisq_contrib[i];

    // Output result - model periodogram meanlog and rmsdlog
    if (output_ps) {
        for (int i=0; i < psl; i++)
            fprintf(outfile_ps, "%.8e\t%.8e\t%.8e\n",
                                freq1*i, ps_meanlog[i], ps_errlog[i]);
        if (!batchmode) printf("Total of %d lightcurves simulated.\n", pcounter);
        fclose(outfile_ps);
    }

    int reject = 0;  // reset rejection rate (no. of models with chisq < obs)
    // Output result - model periodogram chisq distrib
    for (int i=0; i < sim_max; i++) {
        double pchisq = 0;
        for (int ii=0; ii < psl; ii++) {
            pchisq += (bigpstore[i][ii])*(bigpstore[i][ii]);  // chisq contrib
        }
        if (output_chisq) fprintf(outfile_chisq, "%.8e\n", pchisq);
        if (pchisq < lc_chisq) reject++;  // For evaluating rejection%
    }
    if (output_chisq) fclose(outfile_chisq);

    // Output result - save residual distrib at select frequencies
    if (output_psdist) {
        for (int i=0; i < psl/100; i++)
            for (int ii=0; ii < sim_max; ii++)
                fprintf(outfile_psdist, "%.8e\t%.8e\n", freq1*i, bigpstore[ii][i]);

        fclose(outfile_psdist);
    }


    if (!batchmode) printf("Rejection probability: %e\n", 1.*reject/sim_max);


    //*************************************************************************
    // Shutting down....
    // Free memory - every malloc should have a corresponding free/fftw_free
    fftw_free(ts);
    fftw_free(fs);
    if (use_window) free(wnd);
    fftw_free(sim_ps_full);
    fftw_free(sim_lc_full);
    free(ampl);
    fftw_free(lc_out);
    fftw_free(ps_out);
    free(ps_out_ampl);
    if (pdfadjust) {
        fftw_free(pdfsim_lc);
        free(pdfsim_lc_sorted);
        free(pdfsim_lc_rank);
        fftw_free(pdfsim_ps);
    }
    free(ps_meanlog);
    free(ps_errlog);
    free(lc_chisq_contrib);
    free(freq);
    for (int i=0; i < sim_max; i++) free(bigpstore[i]);
    free(bigpstore);


    // fftw_cleanup_threads();


    return 1.0*reject/sim_max;
}
