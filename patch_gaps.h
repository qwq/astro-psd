#ifndef PATCH_GAPS_H_
#define PATCH_GAPS_H_

#include <cstdlib>
#include <cmath>

int find_gaps(double* x, int L, int** gaps);
int patch_line(double* x, double* y, double flag, int L);

#endif  // PATCH_GAPS_H_
