#ifndef LCPDF_H_
#define LCPDF_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "files.h"  // void readline(string line, double** fields, char delim, int length);


struct datapair {
    double val;
    int inx;
};
bool pdfsortfunc(datapair one, datapair two);
void rankcounts(double* counts, int* ranking, int length);
class CDFTable {
    private:
        double* CDF;
        double* RAN;
        int length;
        int bisection(int head, int tail, double val);

    public:
        CDFTable(const char *filename);
        ~CDFTable();
        double randflux(double cdf);
        void fillrandflux(double* output, int length);
};

#endif  // LCPDF_H_
