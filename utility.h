#ifndef UTILITY_H_
#define UTILITY_H_

double mean(double* array, int N);
double variance(double* array, double mean, int N);

#endif  // UTILITY_H_
