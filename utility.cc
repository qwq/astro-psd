/*
utility.cc

Some utilities...
*/
#include "utility.h"

double mean(double* arr, int N)
{
    double sum = 0;
    for (int i=0; i < N; i++) sum += arr[i];
    return sum/N;
}

double variance(double* arr, double mean, int N)
{
    double sqsum = 0;
    for (int i=0; i < N; i++) sqsum += (arr[i])*(arr[i]);
    return (sqsum/N - mean*mean);
}
