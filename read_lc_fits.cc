/*
read_lc_fits.cc

For reading data from FITS files.
*/
#include "read_lc_fits.h"

int read_lc_fits(const char* filename, double** t, double** f)
{
    using namespace std;
    /* Read in light curve from FITS file */
    fitsfile *fptr;//, *outfptr;
    //char card[FLEN_CARD];
    int status = 0, nkeys; // MUST initialize status
    // Check output & input files OK
    //fits_create_file(&outfptr, outfile, &status);
    fits_open_file(&fptr, filename, READONLY, &status);
    if (status)
    {
        fits_report_error(stderr, status);
        return status;
    }

    fits_get_hdrspace(fptr, &nkeys, NULL, &status);
    //printf("Reading file %s\n\n", filename);
    /*
    for (int ii = 1; ii <= nkeys; ii++)
    {
        fits_read_record(fptr, ii, card, &status);
        printf("%s\n", card);
    }
    printf("END\n\n");
    */
    int lc_length;
    fits_read_key(fptr, TLONG, "NAXIS1", &lc_length, NULL, &status);
    // Determine how much memory to grab
    int bitpix;
    fits_get_img_type(fptr, &bitpix, &status);

    if (bitpix > 0)
    {
        printf("Data type of FLOAT_IMG or DOUBLE_IMG expected. Stopping.\n");
        return 1;
    }
    *t = static_cast<double*>(malloc(lc_length*sizeof(double)));
    *f = static_cast<double*>(malloc(lc_length*sizeof(double)));

    /*
    printf("Memory 2 x %d x %d = %d bytes.\n",lc_length,
                            element_size,2*lc_length*element_size);
    */
    // Read input FITS data
    double* fitsdata = static_cast<double*>(malloc(2*lc_length*sizeof(double)));
    long fpixel[2] = {1, 1};
    fits_read_pix(fptr, TDOUBLE, fpixel,  2*lc_length, NULL, fitsdata,
                  NULL, &status);
    if (status)
    {
        fits_report_error(stderr, status);
        return status;
    }

    // Store
    for (int j=0; j<lc_length; j++)
    {
        (*t)[j] = fitsdata[j];
        (*f)[j] = fitsdata[lc_length+j];
    }
    free(fitsdata);
    fits_close_file(fptr, &status);

    return lc_length;
}

