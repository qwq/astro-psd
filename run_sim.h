#ifndef RUN_SIM_H_
#define RUN_SIM_H_

//******************************
// Forward declared dependencies
struct sim_config;
struct model_config;
double lc_sim(sim_config*, model_config*);
// *****************************


//******************************
// Included dependencies
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include "windows.h"  // Data window functions
#include "patch_gaps.h"  // Data patching function
#include "psd_mod.h"  // PSD model functions
#include "fitsio.h"  // int fits_is_reentrant(void); from CFITSIO
#include "lc_sim.h"  // sim_config, model_config
//******************************

#endif  // RUN_SIM_H_
