/*
windows.cc

Data windows for FT spectrum analysis.
*/
#include "windows.h"

/*
Function: wnd_blackman_harris
    The Blackman Harris window function, normalizaed using coherent gain of
    0.359.
    Rolloff rate -60 dB/dec.
    Input: length L of the window
    Output: pointer to the window array
*/
double* wnd_blackman_harris(int L)
{
    double* wnd = static_cast<double*>(malloc(sizeof(double)*L));
    double phase = 2*M_PI/L;
    for (int i=0; i < L; i++)
        wnd[i] = (0.35875 - 0.48829*cos(i*phase)
                    + 0.14128*cos(2*i*phase)
                    - 0.01168*cos(3*i*phase))/0.359;  // 0.50790093025116623;
    return wnd;
}


/*
Function: wnd_hann
    The Hann window functions. Normalized to coherent gain of 0.5.
    Sidelobe -31 dB. Rolloff rate -60 dB/dec.
*/
double* wnd_hann(int L)
{
    double* wnd = static_cast<double*>(malloc(sizeof(double)*L));
    double phase = 2.*M_PI/L;
    for (int i=0; i < L; i++)
        wnd[i] = 0.5*(1.-cos(1.*i*phase))/.5; // 0.375;
    return wnd;
}


/*
Function: wnd_planck
  Planck taper window function, normalized such that the modulus square of its
  discrete Fourier transform has unit area (preserves periodogram power).
  McKechan+ 2010 (A tapering window for time-domain templates...)
  Input: length L of window, delt the tapering length coefficient
  Output: pointer to array with the window function
*/
double* wnd_planck(int L, double delt)
{
    double* wnd = static_cast<double*>(malloc(sizeof(double)*L));
    int bracket = ceil(delt*L); // Number of elements each side to taper
    for (int i=0; i < bracket; i++)
        wnd[i] = 1./(exp(1.*bracket/i+1.*bracket/(i-bracket))+1.);
    for (int i=bracket; i < (L-bracket); i++)
        wnd[i] = 1.;
    for (int i=(L-bracket); i < L; i++)
        wnd[i] = 1./(exp(-1.*bracket/(i-L+bracket)-1.*bracket/(L-i))+1.);
    // Do Fourier transform and find normalisation
    double* ftin = static_cast<double*>(fftw_malloc(sizeof(double)*L));
    fftw_complex* ftout = static_cast<fftw_complex*>(fftw_malloc(sizeof(fftw_complex)*L));
    fftw_plan dofft = fftw_plan_dft_r2c_1d(L, ftin, ftout, FFTW_MEASURE);
    for (int i=0; i < L; i++) ftin[i] = wnd[i];
    fftw_execute(dofft);
    int ftoutlen = floor(L/2)+1;
    double sqsum = 0;
    for (int i=1; i < ftoutlen; i++)
        sqsum += ftout[i][0]*ftout[i][0] + ftout[i][1]*ftout[i][1];
    sqsum *= 2; // To include negatives freqs
    sqsum += ftout[0][0]*ftout[0][0] + ftout[0][1]*ftout[0][1];
    sqsum = sqrt(sqsum)/L;
    for (int i=0; i < L; i++) wnd[i] /= sqsum;
    fftw_free(ftin);
    fftw_free(ftout);
    return wnd;
}
