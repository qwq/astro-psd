#ifndef BINDAT_H_
#define BINDAT_H_

#include <cstdlib>
#include <cmath>

double* logbingrid(double* x, double* y, double L, double* grid, int gridL);

#endif  // BINDAT_H_
