#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include "fitsio.h"
#include <omp.h>

/*
    struct_func.cc

    Compute the structure function of time series data.

    Input: FITS file extension of type ImageHDU, with dimensions
    (timepoints, 2). The times are taken from [*,0] and the fluxes are taken
    from [*,1]. Data type should be FLOAT or DOUBLE (BITPIX < 0).

    Output FITS file has (time bins, 2) dimensions in a single ImageHDU.

    Set the binsize, in same unit of time as data, in code.

    Qian Wang                                        10/3/2014
*/

using namespace std;

double omp_get_wtime(void);
const long num_threads = 8;

int main()
{
    double binsize = 0.0625; // 1.5 hr bins

    // For doing a partial job
    long ii_start = 0;
    long ii_end = 0; // Needs to > ii_start to have effect

    const char* lcfile = "zw229_filtered_lc.fits";
    const char* outfile = "!Zw229-lightcurve-SF.fits";
    fitsfile *fptr, *outfptr;
    char card[FLEN_CARD];
    int status = 0, nkeys, ii; // MUST initialize status

    // Check output & input files OK
    fits_create_file(&outfptr, outfile, &status);
    fits_open_file(&fptr, lcfile, READONLY, &status);
    if (status)
    {
        fits_report_error(stderr, status);
        return status;
    }

    fits_get_hdrspace(fptr, &nkeys, NULL, &status);
    printf("Reading file %s\n\n", lcfile);

    for (ii = 1; ii <= nkeys; ii++)  {
        fits_read_record(fptr, ii, card, &status);
        printf("%s\n", card);
    }
    printf("END\n\n");

    long lc_length;
    fits_read_key(fptr, TLONG, "NAXIS1", &lc_length, NULL, &status);
    long total_pairs = (lc_length-1)*lc_length/2;
    printf("Number of data points: %ld\n", lc_length);
    printf("This routine scales as N^2, so it will compare\n");
    printf(">>>  %ld  <<<\n pairs of data!\n",total_pairs);

    // Determine how much memory to grab
    int bitpix;
    int element_size;
    fits_get_img_type(fptr, &bitpix, &status);

    if (bitpix > 0)
    {
        printf("Data type of FLOAT_IMG or DOUBLE_IMG expected. Stopping.\n");
        return 1;
    }
    element_size = sizeof(double);
    double* dat_array = (double*) malloc(2*lc_length*sizeof(double));

    printf("Memory 2 x %ld x %d = %ld bytes.\n",lc_length,
                            element_size,2*lc_length*element_size);

    // Read input FITS data
    double* fitsdata = (double*) malloc(2*lc_length*sizeof(double));
    long fpixel[2];
    fpixel[0] = 1;
    fpixel[1] = 1;
    fits_read_pix(fptr, TDOUBLE, fpixel,  2*lc_length, NULL, fitsdata,
                  NULL, &status);
    if (status)
    {
        fits_report_error(stderr, status);
        return status;
    }

    // Store in dat_array
    for (long i=0; i<2; i++)
    {
        for (long j=0; j<lc_length; j++)
        {
            //printf("%f\n",fitsdata[i]);
            dat_array[2*j+i] = fitsdata[i*lc_length+j]; // Make interleave
        }
    }

    free(fitsdata);
    fits_close_file(fptr, &status);

    // Time lag & output binning
    long nbins = ceil((dat_array[2*(lc_length-1)] - dat_array[0]) / binsize);
    double* sf = (double*) calloc(nbins,sizeof(double));
    long* sfN = (long*) calloc(nbins,sizeof(long));

    double c_start = omp_get_wtime(); // Start timer

    long pair_num = 0;

    // Adjust iter range if needed
    ii_start = (ii_start>=0 && ii_start<(lc_length-1))? ii_start : lc_length-1;
    ii_end = (ii_end>ii_start && ii_end<=(lc_length-1))? ii_end : lc_length-1;

    omp_set_num_threads(num_threads);
    #pragma omp parallel for schedule(guided,1000) shared(sf,sfN,pair_num,dat_array)
    for (long ii=ii_start;ii<ii_end;ii++)
    {
        double t1 = dat_array[2*ii];
        double f1 = dat_array[2*ii+1];
        double dt;
        double f2;

        for (long jj=ii+1;jj<lc_length;jj++)
        {
            dt = dat_array[2*jj] - t1; // Lag time
            f2 = dat_array[2*jj+1];
            long ibin = floor(dt / binsize);
            #pragma omp atomic
            sf[ibin] += (f2-f1)*(f2-f1); // SF contrib
            #pragma omp atomic
            sfN[ibin] += 1; // Increase count by 1

            // Progress stat
            #pragma omp atomic
            pair_num++;

            if (pair_num % 1000000 == 0) {
                double progress = ((double) pair_num)/total_pairs;
                printf("%.3f%% - %ld s remaining    \r",
                    100*progress,
                    (long) ((omp_get_wtime()-c_start)/progress*(1-progress)));
            }
        }
    }

    double* sfNd = (double*) malloc(nbins*sizeof(double));
    for (long kk=0; kk<nbins; kk++)
    {
        sf[kk] /= sfN[kk]; // Normalize
        sfNd[kk] = (double) sfN[kk];
        //printf("%f",sf[kk]);
    }

    long naxes[2] = {nbins, 2};
    long fpixel2[2] = {1, 1};
    fits_create_img(outfptr, -64, 2, naxes, &status);
    fits_write_pix(outfptr, TDOUBLE, fpixel2, nbins, sf, &status);
    fpixel2[1] = 2;
    fits_write_pix(outfptr, TDOUBLE, fpixel2, nbins, sfNd, &status);

    if (status)          /* print any error messages */
    {
        fits_report_error(stderr, status);
        return status;
    }

    fits_close_file(outfptr, &status);

    printf("\n");
    return(status);
}

