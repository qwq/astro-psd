/*
run_sim.cc
*/

#include "run_sim.h"

using namespace std;

/*
    Set parameters for simulation below, and compile.
    Spawn multiple instances of this executable and pipe commands to it:

    ./run_sim log10(brk_f) log10(brk_p) low_slope high_slope 1

    The last argument sets batchmode=1, so only the rejection rate is printed.
    This file also disables file output when batchmode=1. Otherwise output is
    verbose, and some result files are created. Default settings are found in
    the declaration of the structs model_config and sim_config (lc_sim.h).
*/
int main(int argc, char* argv[])
{

    //************************************************************************
    // Check that CFITSIO was compiled with --enable-reentrant to enable
    // multithreading
    if (!fits_is_reentrant())
    {
        printf("Error: CFITSIO must be compiled with -D_REENTRANT "
                    "compiler directive.\n");
        return 1;
    }



    //******
    // Load config data structures
    sim_config batchconfig_sim;
    sim_config* simcf = &batchconfig_sim;
    model_config batchconfig_model;
    model_config* modcf = &batchconfig_model;


    //******
    // Take cmdline input as model parameters
    // Default values used if not specified as arguments
    double brk_f = -5.5; // log(f_break)
    double brk_p = 2.8; // log(p_break)-log(p_noise)
    double low_a = 2; // low freq slope
    double high_a = 3.9; // high freq slope

    if (argc == 5 || argc == 6) // eg. for when process spawned by batch
    {
        brk_f = atof(argv[1]);
        brk_p = atof(argv[2]);
        low_a = atof(argv[3]);
        high_a = atof(argv[4]);
    }
    if (argc >= 6)
    {
        if (atoi(argv[5]) == 1)
        {
            // Batch mode output options
            simcf->batchmode = 1;   // Only output the rejection rate...
            simcf->output_chisq = 0;  // Turn off file output!
            simcf->output_ps = 0;     // Don't want to do disk i/o
            simcf->output_psdist = 0; // when running 1000's of
            simcf->output_inpsd = 0;  // models!
            simcf->output_phase = 0;
        }
    }
    else
    {
        // Non batchmode output options
        simcf->batchmode = 0;
        simcf->output_chisq = 1;  // Turn off file output!
        simcf->output_ps = 1;     // Don't want to do disk i/o
        simcf->output_psdist = 0; // when running 1000's of
        simcf->output_inpsd = 1;  // models!
        simcf->output_phase = 0;
    }


    //******
    // Customize
    simcf->use_window = 1;
    simcf->pdfadjust = 0;
    simcf->lcfile = "binned_lc/zw229_lc_new_4hr.fits";
    //simcf->lcfile = "ka1858_lc_4hr.fits";
    simcf->tbin = 3600*4;


    simcf->lcpdffile = "zw229_new_lc_2hr_pdf.dat";

    simcf->sim_max = 1000;
    simcf->rndsd = 10001;
    simcf->rnl = 100;
    //simcf->wnd_func = &wnd_blackman_harris;
    simcf->wnd_func = &wnd_hann;
    simcf->gap_func = &patch_line;
    modcf->func = &psd_brkn_powerlaw;
    modcf->pars[0] = pow(10, brk_p - 1.1);
    modcf->pars[1] = pow(10, brk_f);
    modcf->pars[2] = low_a;
    modcf->pars[3] = high_a;
    modcf->pars[4] = pow(10, -1.1);
    //******

    double rejection = lc_sim(simcf, modcf);
    if (simcf->batchmode)
    {
        printf("%f", rejection);
    }
    else
    {
        printf("%f\t%f\t%f\t%f\t%f\n", brk_f, brk_p, low_a, high_a, rejection);
    }

    /*
    The following for loop goes through a predefined set of permutations of
    the model parameters, as a brute-force coarse initial search.
    It is better to run this program as multiple independent processes
    each computing one set of parameters, and manage these threads in parallel
    with a parameter controller.
    */
    /*
    FILE *outfile_simres = prep_outfile("4hr_results.dat");
    for (int ff=0;ff<10;ff++)
    {
        for (int hh=0; hh<1; hh++)
        {
            for (int pp=0; pp<10; pp++)
            {
                brk_f=0.05*ff-6.0;
                high_a=0.1*hh+3.5;
                brk_p=0.1*pp+2.5;
                double rejection = lc_sim(tbin, sim_max, rndsd, rnl, brk_f, brk_p, low_a, high_a,lcfile, lcpdffile);
                fprintf(outfile_simres, "%f\t%f\t%f\t%f\t%f\n",brk_f, brk_p, low_a, high_a,rejection);
                fflush(outfile_simres);
            }
        }
    }
    fclose(outfile_simres);
    */

    return 0;
}


