#ifndef PSD_MOD_H_
#define PSD_MOD_H_

#include <cstdlib>
#include <cmath>

int psd_brkn_powerlaw(double* f, double* p, int L, double* model);

#endif  // PSD_MOD_H_
