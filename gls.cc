#include <omp.h>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include "fenv.h"
#include "fitsio.h"
/*
    gls.cc

    Compute the periodogram of time series data using the generalized Lomb-
    Scargle formalism (Zechmeister & Kuerster 2009).

    Input: FITS file extension of type ImageHDU, with dimensions (timepoints,
    2). The times are taken from [*,0] and the fluxes are taken from [*,1].
    Data type should be FLOAT or DOUBLE (BITPIX < 0).

    Output FITS file has (freq grid, 2) dimensions in a single ImageHDU. The
    first dimension records p(w) and the second records weights.

    Set the frequency grid in code. Default is to go in step size of Pi/T,
    where T is the span of the data, until a frequency that is ~Nyquist
    sampled by the majority of the uniform sampling (*Nyquist sampling does
    not really exist for non-uniform sampling...)


    Qian Wang                                        12/3/2014
*/

namespace std {

double omp_get_wtime(void);
const int num_threads = 8;

int main() {
    /*if (argc < 2 || argc > 3)
    {
        printf("Usage: ./gls input_lc output_spec");
    }*/
    feenableexcept(FE_OVERFLOW | FE_UNDERFLOW | FE_DIVBYZERO);
    const double timedel = 0.02043359821692;  // [d] time resolution of data

    // const char* lcfile = "zw229_filtered_lc.fits";
    // const char* outfile = "!Zw229-lightcurve-GLS.fits";
    const char* lcfile = "fake_lc_const.fits";
    const char* outfile = "!fake_lc_const-GLS.fits";
    fitsfile *fptr, *outfptr;
    char card[FLEN_CARD];
    int status = 0, nkeys, ii;  // MUST initialize status

    // Check output & input files OK
    fits_create_file(&outfptr, outfile, &status);
    fits_open_file(&fptr, lcfile, READONLY, &status);
    if (status) {
        fits_report_error(stderr, status);
        return status;
    }

    fits_get_hdrspace(fptr, &nkeys, NULL, &status);
    printf("Reading file %s\n\n", lcfile);

    for (ii = 1; ii <= nkeys; ii++) {
        fits_read_record(fptr, ii, card, &status);
        printf("%s\n", card);
    }
    printf("END\n\n");

    int64_t lc_length;
    fits_read_key(fptr, TLONG, "NAXIS1", &lc_length, NULL, &status);
    printf("Number of data points: %ld\n", lc_length);

    // Determine how much memory to grab
    int bitpix;
    int element_size;
    fits_get_img_type(fptr, &bitpix, &status);

    if (bitpix > 0) {
        printf("Data type of FLOAT_IMG or DOUBLE_IMG expected. Stopping.\n");
        return 1;
    }
    element_size = sizeof(double);
    double* dat_array = static_cast<double*>(malloc(2*lc_length*sizeof(double)));

    printf("Memory 2 x %ld x %d = %ld bytes.\n", lc_length,
                            element_size, 2*lc_length*element_size);

    // Read input FITS data
    double* fitsdata = static_cast<double*>(malloc(2*lc_length*sizeof(double)));
    int64_t fpixel[2];
    fpixel[0] = 1;
    fpixel[1] = 1;
    fits_read_pix(fptr, TDOUBLE, fpixel, 2*lc_length, NULL, fitsdata,
                  NULL, &status);
    if (status) {
        fits_report_error(stderr, status);
        return status;
    }

    // Store in dat_array
    for (int64_t i=0; i < 2; i++) {
        for (int64_t j=0; j < lc_length; j++) {
            // printf("%f\n",fitsdata[i]);
            dat_array[2*j+i] = fitsdata[i*lc_length+j];  // Make interleave
            // t0, f0, t1, f1, ...etc.
        }
    }

    free(fitsdata);
    fits_close_file(fptr, &status);

    // Compute output frequency grid
    double T = dat_array[2*(lc_length-1)] - dat_array[0];  // Total time
    int64_t N = (int64_t) floor(T/timedel);  // #samples at the sampling rate in T
    double vv0 = M_PI/T;
    double* gls = static_cast<double*>(calloc(N, sizeof(double)));
    double* glsw = static_cast<double*>(calloc(N, sizeof(double)));  // weights

    printf("Total time spanned by time series: %f\n", T);
    printf("At sampling interval %f, number of samples is: %ld\n", timedel, N);
    printf("Base frequency: %.15e\n", M_PI/T);

    double c_start = omp_get_wtime();  // Start timer
    int64_t n = 0;  // progress counter

    omp_set_num_threads(num_threads);
    // No weights currently used!!
    #pragma omp parallel for schedule(guided, 1000) shared(gls, glsw, n, dat_array)
    for (int64_t ii=0; ii < N; ii++) {   // Loop over frequencies
        double vv = vv0 * (ii+1);  // current frequency
        double C, S, Y, CC_, SS_, CS_, YY_, YC_, YS_;
        double CC, SS, CS, YY, YS, YC, D;
        double t, y, c, s;
        double w = 1./lc_length;  // weight

        C = S = Y = CC_ = SS_ = CS_ = YY_ = YC_ = YS_ = 0;
        CC = SS = CS = YY = YS = YC = D = 0;

        for (int64_t jj=0; jj < lc_length; jj++) {  // Loop over time series
            t = dat_array[2*jj];
            y = dat_array[2*jj+1];
            c = cos(vv*t);
            s = sin(vv*t);
            C += w*c;
            S += w*s;
            Y += w*y;
            CC_ += w*c*c;
            SS_ += w*s*s;
            CS_ += w*c*s;
            YY_ += w*y*y;
            YC_ += w*y*c;
            YS_ += w*y*s;
        }
        // printf("%e\t%e\t%e\t%e\t%e\t%e\t%e\n",C,S,Y,CC_,SS_,CS_,YY_);

        CC = CC_ - C*C;
        SS = SS_ - S*S;
        CS = CS_ - C*S;
        YY = YY_ - Y*Y;
        YC = YC_ - Y*C;
        YS = YS_ - Y*S;
        D = CC*SS - CS*CS;
        // printf("%e\t%e\t%e\t%e\t%e\t%e\t%e\n",CC,SS,CS,YY,YC,YS,D);

        gls[ii] = (SS*YC*YC + CC*YS*YS - 2.*CS*YC*YS)/(D*YY);  // periodogram contrib
        #pragma omp atomic
        n++;
        if (n % 100 == 0) {
            double progress = 1.*n/N;
            printf("%.3f%% - %ld s remaining    \r",
                100*progress,
                (int64_t) ((omp_get_wtime()-c_start)/progress*(1-progress)));
        }
    }

    int64_t naxes[2] = {N, 2};
    int64_t fpixel2[2] = {1, 1};
    fits_create_img(outfptr, -64, 2, naxes, &status);
    fits_write_pix(outfptr, TDOUBLE, fpixel2, N, gls, &status);
    fpixel2[1] = 2;
    fits_write_pix(outfptr, TDOUBLE, fpixel2, N, glsw, &status);

    if (status) {         /* print any error messages */
        fits_report_error(stderr, status);
        return status;
    }

    fits_close_file(outfptr, &status);

    printf("\n");
    return(status);
}

}  // namespace std
