/*
files.cc

For preparing output files.
*/
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <sstream>
#include <fstream>

#include "files.h"

// Make sure fields has the right size!
void readline(std::string line, double** fields, char delim, int length) {
    using namespace std;
    istringstream ss(line);    // Put line into stringstream
    string field;
    int i = 0;  // Keep track of field number
    while (getline(ss, field, delim)) {
        if (i == length) {
            printf("Data line has more fields than expected. Terminating!\n");
            printf("Last read line: \n");
            printf("%s", line.c_str());
            exit(EXIT_FAILURE);
        }
        stringstream fs(field);
        fs >> *(fields[i]);
        i++;
    }
    if (i < length) {
        printf("Data line has fewer fields than expected. Terminating!\n");
        printf("Last read line: \n");
        printf("%s", line.c_str());
        exit(EXIT_FAILURE);
    }
}


FILE* prep_outfile(const char* basename, bool append) {
    using namespace std;
    FILE *file;
    bool exists = 1;
    int num = 1;
    string filename;
    filename.append(basename);

    if (append) {
        const char* fname = filename.c_str();
        file = fopen(fname, "a");
    } else {
        // Check if it already exists
        while (exists) {
            // Not overwrite, try filename with a number appended, keep trying
            // until an available filename is reached
            file = fopen(filename.c_str(), "r");
            if (file == NULL) {
                exists = 0;
            } else {
                fclose(file);
                stringstream tem;
                tem << num;
                filename.clear();
                filename.append(basename);
                filename.append(tem.str());
                num++;
            }
        }
        // Filename is available now!
        const char* fname = filename.c_str();
        file = fopen(fname, "w");
    }
    return file;
}


FILE* prep_outfile(const char* basename) {
    return prep_outfile(basename, 0);
}
