#ifndef LC_SIM_H_
#define LC_SIM_H_

/* lc_sim.h */
/*
   Default simulation and model settings are found in the structs model_config
   and sim_config below.
 */

#include <unistd.h>
#include <sys/wait.h>
#include <omp.h>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cmath>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>
#include <complex>
#include <vector>

#include "fitsio.h"
#include "fftw3.h"

#include "normal.hpp"

/*
prep_outfile, readline
*/
#include "files.h"

/*
CDFTable
*/
#include "lcpdf.h"

/*
find_gaps
*/
#include "patch_gaps.h"

/*
int read_lc_fits(const char* filename, double** t, double** f);
*/
#include "read_lc_fits.h"

/*
double mean(double* array, int N);
double variance(double* array, double mean, int N);
*/
#include "utility.h"


struct sim_config {
    bool batchmode;  //= 0;
    // Output files:
    bool output_chisq;  //= 1;      // chisquare distribution of realizations vs model
    bool output_ps;  //= 1;         // realization-averaged periodogram
    bool output_psdist;  //= 0;     // distribution of PSD at set frequencies (big!)
    bool output_inpsd;  //= 1;      // periodogram of input lightcurve
    bool output_phase;  //= 0;      // realization-averaged phase
    bool use_window;  //= 1;
    bool pdfadjust;  //= 0;         // Adjust lightcurve PDF
    // (wnd array) = (in arraylength)
    // double* (*wnd_func)(int) = &wnd_blackman_harris;
    double* (*wnd_func)(int);       //= &wnd_hann;
    // (no. of gaps) = (in x, in y, in flagval, in arraylength)
    int (*gap_func)(double*, double*, double, int);  //= &patch_line;
    int sim_max;  //= 10000;        // segments of lightcurve to simulate
    int rnl;  //= 1000;             // full lightcurve simulated is rnl*L long
    const char* lcfile;
    const char* lcpdffile;
    double tbin;                    // time bin of input lightcurve
    int rndsd;  //= 10000;          // starting random seed
};

struct model_config {
    // (in freq, in model_pars, in arraylength, out PSD)
    int (*func)(double*, double*, int, double*);  //= &psd_brkn_powerlaw;
    double pars[5];
};

double lc_sim(sim_config* sc, model_config* mc);

#endif  // LC_SIM_H_
