#ifndef WINDOWS_H_
#define WINDOWS_H_

#include <cstdlib>
#include <cmath>
#include <fftw3.h>

double* wnd_blackman_harris(int L);
double* wnd_hann(int L);
double* wnd_planck(int L, double delt);

#endif  // WINDOWS_H_
