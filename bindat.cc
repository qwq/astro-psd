/*
bindat.cc

Data binning utilities
*/
#include "bindat.h"
/*
    Function: logbingrid
    Bin the input data array y values, according to defined grid.
    Binned values are the mean of log10(data) in that bin, ie. geometric mean.
    Input:
    x - abssica, must be ordered
    y - corresponding values
    L - length of x and y arrays
    grid - an array containing the bin boundaries
    gridL - length of grid
    Output:
    pointer to an array of length len(grid)-1, containing mean(log(y)) of bins.

    Warning:
    Input y is not checked for negative values, which trigger errors with log.
*/
double* logbingrid(double* x, double* y, double L, double* grid, int gridL) {
    double ct, logsum, bin2;
    int flag;
    double* binned = static_cast<double*>(std::malloc(sizeof(double)*(gridL-1)));
    flag = 0;
    while (x[flag] < grid[0]) flag++;  // Advance to first bin position
    for (int i=1; i < gridL; i++) {
        ct = 0;
        logsum = 0;
        bin2 = grid[i];
        while (x[flag] < bin2) {
            logsum += log10(y[flag]);
            ct++;
            flag++;
        }
        binned[i-1] = ct > 0 ? logsum/ct : 0;
    }
    return binned;
}

// double* logbinsize(double* x, double* y, double L, double xstart, double xend, double factor, double* outLength);
// double* logbinnum(double* x, double* y, double L, double xstart, double xend, double nbins, double* outLength);
