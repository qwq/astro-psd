#ifndef READ_LC_FITS_H_
#define READ_LC_FITS_H_

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "fitsio.h"

int read_lc_fits(const char* filename, double** t, double** f);

#endif  // READ_LC_FITS_H_
