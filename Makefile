SHELL =		/bin/sh
CFITSIO =	./cfitsio
FFTW = /usr/local/Cellar/fftw/3.3.6-pl2
CPP =		/usr/local/Cellar/gcc/7.2.0/bin/g++-7 -I$(CFITSIO) -I$(FFTW)/include
ICPC = icpc -I$(CFITSIO)
LIBS =		-lcfitsio -lm -lpthread -fopenmp
FITSIO = readfits.o writefits.o printerr.o writeheader.o

MAIN = structfunc.cc
LCSIM = normal.cpp read_lc_fits.cc patch_gaps.cc windows.cc psd_mod.cc files.cc lcpdf.cc utility.cc lc_sim.cc run_sim.cc


all: $(MAIN)
	${CPP} -o structfunc $(MAIN) -O2 -L$(CFITSIO) $(LIBS)

fast: $(MAIN)
	${CPP} -o structfunc $(MAIN) -O2 -L$(CFITSIO) $(LIBS)

intel: $(MAIN)
	${ICPC} -o structfunc $(MAIN) -O2 -L$(CFITSIO) $(LIBS)

debug: $(MAIN)
	${CPP} -o structfunc $(MAIN) -g -L$(CFITSIO) $(LIBS)

gls: gls.cc
	${CPP} -o gls gls.cc -O2 -L$(CFITSIO) $(LIBS)

lc: $(LCSIM)
	${CPP} -std=c++11 -Wall -o run_lcsim $(LCSIM) -L$(CFITSIO) -L$(FFTW)/lib -lfftw3_threads -lfftw3 $(LIBS) -O2
	${CPP} -Wall -o run_lcsim_helper run_sim_helper.cc -O2
	${CPP} -Wall -o map_modelfit normal.cpp files.cc map_rejection.cc -O2

lcastro: $(LCSIM)
	${CPP} -std=c++0x -Wall -o run_lcsim $(LCSIM) -L/home/qw/lib -L$(CFITSIO) -L$(FFTW)/lib -lfftw3_threads -lfftw3 $(LIBS) -O2
	${CPP} -Wall -o run_lcsim_helper run_sim_helper.cc -O2
	${CPP} -Wall -o map_modelfit normal.cpp files.cc map_rejection.cc -O2


lcscorpius: $(LCSIM)
	${CPP} -Wall -o map_modelfit.scorpius normal.cpp files.cc map_rejection.scorpius.cc -O2

lccygnus: $(LCSIM)
	${CPP} -Wall -o map_modelfit.cygnus normal.cpp files.cc map_rejection.cygnus.cc -O2

lcneptune: $(LCSIM)
	${CPP} -Wall -o map_modelfit.neptune normal.cpp files.cc map_rejection.neptune.cc -O2

lcvirgo: $(LCSIM)
	${CPP} -Wall -o map_modelfit.virgo normal.cpp files.cc map_rejection.virgo.cc -O2
	${CPP} -std=c++0x -Wall -o run_lcsim100 normal.cpp read_lc_fits.cc patch_gaps.cc windows.cc psd_mod.cc files.cc lcpdf.cc utility.cc lc_sim.cc run_sim100.cc -L/home/qw/lib -L$(CFITSIO) -lfftw3_threads -lfftw3 $(LIBS) -O2
	${CPP} -Wall -o run_lcsim_helper100 run_sim_helper100.cc -O2

tt:
	${CPP} -Wall -o reader normal.cpp files.cc map_rejection.cc -O2
	${CPP} -Wall -o writer test_child_node.cc -O2

