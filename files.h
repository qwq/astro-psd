#ifndef FILES_H_
#define FILES_H_

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>


void readline(std::string line, double** fields, char delim, int length);
FILE* prep_outfile(const char* basename, bool append);
FILE* prep_outfile(const char* basename);


#endif  // FILES_H_
