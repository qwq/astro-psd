/*
lcpdf.cc

For handling user-specified lightcurve PDF file and generating white noise
lightcurve with this PDF.
*/

#include "lcpdf.h"

bool pdfsortfunc(datapair one, datapair two) {
    return one.val < two.val;
}

void rankcounts(double* counts, int* ranking, int length) {
    datapair* orderlist = static_cast<datapair*>(malloc(sizeof(datapair)*length));
    for (int i=0; i < length; i++) {
        orderlist[i].val = counts[i];
        orderlist[i].inx = i;
    }

    std::sort(orderlist, orderlist+length, pdfsortfunc);

    for (int i=0; i < length; i++) {
        ranking[i] = orderlist[i].inx;
    }

    free(orderlist);
}

/* Implementations for CDFTable class */
int CDFTable::bisection(int l, int r, double val) {
    int m;
    while (r > l) {
        m = l + (r-l+1)/2;
        if (this->CDF[m] > val) {
            r = m-1;
        } else {
            l = m;
        }
    }
    return l;
}

CDFTable::CDFTable(const char *filename) {
    std::ifstream infile(filename);
    std::string line;
    int linecount = 0;
    while (getline(infile, line))  // Read all lines
        linecount++;
    infile.clear();
    infile.seekg(0);
    this->CDF = static_cast<double*>(malloc(sizeof(double)*linecount));
    this->RAN = static_cast<double*>(malloc(sizeof(double)*linecount));
    int i = 0;
    double field1;
    double field2;
    double *f[2] = {&field1, &field2 };  // Store pointers for fields
    while (getline(infile, line)) {
        readline(line, f, '\t', 2);
        this->RAN[i] = field1;
        this->CDF[i] = field2;
        i++;
    }
    printf("Loaded %d lines of data for PDF function...\n", linecount);
    this->length = linecount;
}

CDFTable::~CDFTable() {
    free(this->CDF);
    free(this->RAN);
}

double CDFTable::randflux(double cdf) {
    // Bisection search
    int x1 = this->bisection(0, (this->length)-1, cdf);
    // Linear interpolation
    double prop = (cdf-this->CDF[x1])/(this->CDF[x1+1]-this->CDF[x1]);
    return this->RAN[x1]+prop*(this->RAN[x1+1]-this->RAN[x1]);
}

void CDFTable::fillrandflux(double* output, int length) {
    for (int i=0; i < length; i++) {
        output[i] = this->randflux(1.0*rand()/RAND_MAX);
    }
}
